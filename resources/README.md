# Resources

The Cellaro API is a JSON API.

Guiding principles are:

* Always return JSON.
* Utilize HTTP error codes and methods.
* In general, required parameters are in URLs; optional parameters are specified in the query string. **This is not always the case.**
* If we need complex data structures from you, you should send them as a JSON string. We don't need any more conventions for putting arrays and dictionaries directly into URL-encoded GET/POST values.
* We follow a convention of including the API version number in the resource path. API calls of version 0 are subject to change throughout the process. Once we promote something to version 1, we hope to keep its implementation stable.

## Hostname
Please use https://cellaroserver.com/ to access the APIs.

# Table of Contents

* [Schedule](#schedule)
* [Events](#events)
* [Files](file/endpoints/)
* [Streaming API](#streaming-api)

## HTTP Endpoints

### [Schedule](schedule.md)

| Path | Protocol | Method | Description |
| ---- | -------- | ------ | ----------- |
| /api/v0/schedule | HTTP/S | GET | [Retrieve a Schedule](schedule.md#retrieve-a-schedule) |

### [Events](events.md)

| Path | Protocol | Method | Description |
| ---- | -------- | ------ | ----------- |
| /api/v0/programs/:program_id/events | HTTP/S | GET | [Retrieve an Events list](events.md#retrieve-an-events-list) |
| /api/v0/programs/:program_id/events | HTTP/S | POST | [Create an Events list](events.md#create-an-events-list) |

# Streaming API

| Path | Protocol | Description |
| ---- | -------- | ----------- |
| /stream/v0/:resource | Web Socket | [Use the Streaming API](streaming.md) |
