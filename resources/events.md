# Events

## Create an Events list

Create a new Events list composed of [Event](/objects.md#event) objects that will be associated with a [Program](/objects.md#program).

### URL
> https://api.cellaroserver.com/v0/programs/:program_id/events

### Data

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `events` | Required | list | A list of [Event](/objects.md#event) objects to be associated with a [Program](/objects.md#program).

### Example

> POST https://api.cellaroserver.com/v0/programs/270492450-003C505A9FF2EB56D483276A36D45C96/events
>
> Content-Type: application/json
>
> DATA '[ { "annotations": [ … ], "start": "00:00:04.000", "end": "00:00:09.000" } ]'

``` js
{
    "data": [
        {
            "annotations": [
                {
                    "type": "com.cellaroserver.core.concept",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                },
                {
                    "type": "core.cellaroserver.core.oembed",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                }
            ],
            "end": "00:00:09.000",
            "start": "00:00:04.000"
        }
    ],
    "meta": {
        "code": 200
    }
}
```

## Retrieve an Events list

Retrieve an Events list composed of [Event](/objects.md#event) objects, associated with a [Program](/objects.md#program).

### URL

> https://api.cellaroserver.com/v0/programs/:program_id/events

### Example

> GET https://api.cellaroserver.com/v0/programs/270492450-003C505A9FF2EB56D483276A36D45C96/events

``` js
{
    "data": [
        {
            "annotations": [
                {
                    "type": "com.cellaroserver.core.concept",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                },
                {
                    "type": "core.cellaroserver.core.oembed",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                }
            ],
            "end": "00:00:09.000",
            "start": "00:00:04.000"
        }
    ],
    "meta": {
        "code": 200
    }
}
```

## Delete an Events list

Delete an Events list associated with a [Program](/objects.md#program).

### URL

> https://api.cellaroserver.com/v0/programs/:program_id/events

### Example

> DELETE https://api.cellaroserver.com/v0/programs/270492450-003C505A9FF2EB56D483276A36D45C96/events

``` js
{
    "data": [
        {
            "annotations": [
                {
                    "type": "com.cellaroserver.core.concept",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                },
                {
                    "type": "core.cellaroserver.core.oembed",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                }
            ],
            "end": "00:00:09.000",
            "start": "00:00:04.000"
        }
    ],
    "meta": {
        "code": 200
    }
}
```