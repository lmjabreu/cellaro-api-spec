# Schedule

The Schedule endpoint returns EPG data containing Airings, Channels and Programs.

Reference to any specific data source provider, such as Gracenote, is purposely ommited for better stability. A future/past change of provider would not break any client consuming the data as long as the resource url and schema remain the same.

## Retrieve a Schedule

### URL

> http://api.cellaroserver.com/v0/schedule

### Parameters

| Name        | Required? | Type    | Description |
| ----------- | --------- | ------- | ----------- |
| `language`  | Optional  | string  | Specify the language of the text data as a valid ISO 639-1 language code. Default is `en`. |
| `channel`   | Required  | string  | A comma separated list of Channel IDs. |
| `showcased` | Optional  | boolean | Returns **only** showcased programs when `true`. Default is `false`. |
| `since`     | Optional  | string  | Specify the EPG start time, expressed in [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) format. Default is `now-1`. |
| `before`    | Optional  | string  | Specify the EPG end time, expressed in [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) format. Default is `now+5`. |

### Example

> GET http://api.cellaroserver.com/v0/schedule?channel=251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB

``` js
{
    "data": {
      "airings": [
        {
          "channel_id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB",
          "end": "2012-12-31T12:30:00Z",
          "program_id": "270492450-003C505A9FF2EB56D483276A36D45C96",
          "start": "2012-12-31T10:30:00Z"
        }
      ],
      "channels": [
        {
          "annotations": [
              {
                  "type": "com.cellaroserver.core.oembed",
                  "data": {
                      "the real annotation data": "would go here"
                  }
              }
          ],
          "id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB",
          "name": "Cable News Network",
          "short_name": "CNN"
        }
      ],
      "programs": [
        {
            "annotations": [
                {
                    "type": "com.cellaroserver.core.oembed",
                    "value": {
                        "the real annotation data": "would go here"
                    }
                }
            ],
            "id": "270492450-003C505A9FF2EB56D483276A36D45C96",
            "showcased": true,
            "synopsis": "New York City policeman John McClane (Bruce Willis) is visiting his estranged wife (Bonnie Bedelia) and two daughters on Christmas Eve. He joins her at a holiday party in the headquarters of the Japanese-owned business she works for. But the festivities are interrupted by a group of terrorists who take over the exclusive high-rise, and everyone in it. Very soon McClane realizes that there's no one to save the hostages -- but him.",
            "title": "Die Hard"
        }
      ]
    },
    "meta": {
        "code": 200,
        "max": "2012-12-31T09:22:55Z",
        "min": "2012-12-31T15:22:55Z",
        "more": true
    }
}
```
