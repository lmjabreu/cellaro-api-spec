# File Lifecycle

* [Create a File](#create-a-file)
* [Update a File](#update-a-file)
* [Delete a File](#delete-a-file)

## Create a File

Create a new [File](/resources/file.md).

A Cellaro File object can be created without setting the file content. This is called an "incomplete" file object. To create an incomplete file object, POST a JSON document that matches the [File schema](/resources/file/) with an HTTP header of `Content-Type: application/json`. Currently, the only keys we use from your JSON will be `kind`, `type`, `name` and `annotations`. You can also send those keys as standard form data instead of as JSON. Once you have an incomplete file object, you can [set the file content](/resources/file/endpoints/content.md#set-file-content) in a later request.

You can also create a complete File object with one request by including the file content with the File metadata. To create a complete file object, send a POST with an HTTP header of `Content-Type: multipart/form-data`. Our API expects one part of the request body to contain a `Content-Disposition` header with a value for `filename` and `name="content"`. The data from this part will be used as the file's content. If you wish to send your data as Base64 encoded instead of as a byte stream you must include a `Content-Transfer-Encoding: base64` header. If there is a part with `name="metadata"` and `Content-Type: application/json` then we will parse that JSON as the file's metadata. Otherwise, we will construct the metadata from the `form-data` sent in the request body.

# Endpoint

| Method | Path |
| ------ | ---- |
| POST   | /api/v0/files |

### Data

None.

### Example

```
POST https://api.cellaroserver.com/v0/files

Content-Type: multipart/form-data; boundary=82481319dca6

REQUEST BODY:
--82481319dca6
Content-Disposition: form-data; name="content"; filename="filename.png"
Content-Type: image/png

...contents of file...
--82481319dca6
Content-Disposition: form-data; name="metadata"; filename="metadata.json"
Content-Type: application/json

{"type": "com.example.test"}
```

The metadata can also be submitted as normal post data in which case that part of the request body will look like:

```
--82481319dca6
Content-Disposition: form-data; name="type"

com.example.test
```

You can use the following curl command to upload a file:

> curl -k https://api.cellaroserver.com/v0/files -F 'type=com.example.test' -F content=@filename.png -X POST

``` js
{
    "data": {
        "complete": true,
        "created_at": "2013-01-28T18:31:18Z",
        "derived_files": {
            "image_thumb_200s": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_200s.jpg"
            },
            "image_thumb_960r": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_960r.jpg"
            }
        },
        "id": "deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g",
        "image_info": {
            "height": 480,
            "width": 640
        },
        "kind": "image",
        "mime_type": "image/jpg",
        "name": "filename.jpg",
        "size": 34804,
        "sha1": "0fd0710c09c397bd63a731249f89e0ed0942476e",
        "url": "http://cdn1.example.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g.jpg"
    },
    "meta": {
        "code": 200
    }
}
```

## Update a File

Updates a specific [File](/resources/file/file.md) object. You can update a file by PUTing an object that matches the [File schema](/resources/file.md) with an HTTP header of `Content-Type: application/json`. The only keys that can be updated are `annotations` and `name`.

| Method | Path |
| ------ | ---- |
| PUT    | /api/v0/files/:file_id |

### Data

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `file_id` | Required | string | The id of the File to update |

### Example

> PUT https://api.cellaroserver.com/v0/files/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g
>
> Content-Type: application/json
>
> DATA {"name": "updated_filename.jpg"}

``` js
{
    "data": {
        "complete": true,
        "created_at": "2013-01-28T18:31:18Z",
        "derived_files": {
            "image_thumb_200s": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_200s.jpg"
            },
            "image_thumb_960r": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_960r.jpg"
            }
        },
        "id": "deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g",
        "image_info": {
            "height": 480,
            "width": 640
        },
        "kind": "image",
        "mime_type": "image/jpg",
        "name": "updated_filename.jpg",
        "size": 34804,
        "sha1": "0fd0710c09c397bd63a731249f89e0ed0942476e",
        "url": "http://cdn1.example.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g.jpg"
    },
    "meta": {
        "code": 200
    }
}
```

## Delete a File

Delete a file. It returns the deleted File on success. *Since a File could be referenced by multiple resources we recommend that you don't delete files without confirming they're not in use anymore*

| Method | Path |
| ------ | ---- |
| DELETE | /api/v0/files/:file_id |

### Data

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `file_id` | Required | string | The id of the File to delete |

### Example

> DELETE https://api.cellaroserver.com/v0/files/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g

``` js
{
    "data": {
        "complete": true,
        "created_at": "2013-01-28T18:31:18Z",
        "derived_files": {
            "image_thumb_200s": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_200s.jpg"
            },
            "image_thumb_960r": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_960r.jpg"
            }
        },
        "id": "deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g",
        "image_info": {
            "height": 480,
            "width": 640
        },
        "kind": "image",
        "mime_type": "image/jpg",
        "name": "filename.jpg",
        "size": 34804,
        "sha1": "0fd0710c09c397bd63a731249f89e0ed0942476e",
        "url": "http://cdn1.example.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g.jpg"
    },
    "meta": {
        "code": 200
    }
}
```
