# File Lookup

* [Retrieve a File](#retrieve-a-file)
* [Retrieve multiple Files](#retrieve-multiple-file)

## Retrieve a File

Returns a specific [File](/docs/resources/file.md).

| Method | Path |
| ------ | ---- |
| GET    | /api/v0/files/:file_id |

### Parameters

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `file_id` | Required | string | The file id. |

### Example

> GET https://api.cellaroserver.com/v0/files/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g

``` js
{
    "data": {
        "complete": true,
        "created_at": "2013-01-28T18:31:18Z",
        "derived_files": {
            "image_thumb_200s": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_200s.jpg"
            },
            "image_thumb_960r": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_960r.jpg"
            }
        },
        "id": "deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g",
        "image_info": {
            "height": 480,
            "width": 640
        },
        "kind": "image",
        "mime_type": "image/jpg",
        "name": "filename.jpg",
        "size": 34804,
        "sha1": "0fd0710c09c397bd63a731249f89e0ed0942476e",
        "url": "http://cdn1.example.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g.jpg"
    },
    "meta": {
        "code": 200
    }
}
```

## Retrieve multiple Files

Returns multiple Files requested by id. At most 200 files can be requested.

| Method | Path |
| ------ | ---- |
| GET    | /api/v0/files |

### Parameters

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `ids` | Required | string | A comma separated list of File ids to retrieve. |

### Example

> GET https://api.cellaroserver.com/v0/files?ids=1,2,3

``` js
{
    "data": [
      "file 1 object": "would go here",
      "file 2 object": "would go here",
      "file 3 object": "would go here"
    ],
    "meta": {
        "code": 200
    }
}
```
