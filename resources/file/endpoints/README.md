# File Endpoints

<table>
    <thead>
        <tr>
            <th width="410">Description</th>
            <th width="80">Method</th>
            <th width="320">Path</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="lifecycle.md#create-a-file">Create a File</a></td>
            <td>POST</td>
            <td>/api/v0/files</td>
        </tr>
        <tr>
            <td><a href="lookup.md#retrieve-a-file">Retrieve a File</a></td>
            <td>GET</td>
            <td>/api/v0/files/:file_id</td>
        </tr>
        <tr>
            <td><a href="lookup.md#retrieve-multiple-files">Retrieve multiple Files</a></td>
            <td>GET</td>
            <td>/api/v0/files</td>
        </tr>
        <tr>
            <td><a href="lifecycle.md#delete-a-file">Delete a File</a></td>
            <td>DELETE</td>
            <td>/api/v0/files/:file_id</td>
        </tr>
        <tr>
            <td><a href="lifecycle.md#update-a-file">Update a File</a></td>
            <td>PUT</td>
            <td>/api/v0/files/:file_id</td>
        </tr>
        <tr>
            <td><a href="content.md#get-file-content">Get File content</a></td>
            <td>GET</td>
            <td>/api/v0/files/:file_id/content</td>
        </tr>
        <tr>
            <td><a href="content.md#set-file-content">Set File content</a></td>
            <td>PUT</td>
            <td>/api/v0/files/:file_id/content</td>
        </tr>
    </tbody>
</table>