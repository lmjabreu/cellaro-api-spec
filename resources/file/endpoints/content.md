# File Content

* [Get File content](#get-file-content)
* [Set File content](#set-file-content)

## Get File content

Get the content for a complete File.

This endpoint will return a 302 Redirect to a temporary URL for the content of this file. This endpoint is useful for fetching private content from a file.

| Method | Path |
| ------ | ---- |
| GET    | /api/v0/files/:file_id/content |

### URL Parameters

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `file_id` | Required | string | The file id. |

## Set File content

Set the content for an incomplete File. The content type for this request must be the content type of the file you are uploading.

| Method | Path |
| ------ | ---- |
| PUT    | /api/v0/files/:file_id/content |

### URL Parameters

| Name | Required? | Type | Description |
| ---- | --------- | ---- | ----------- |
| `file_id` | Required | string | The file id. |

### Example

> PUT https://api.cellaroserver.com/v0/files/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g/content
>
> Content-Type: image/jpeg
>
> DATA [raw file, not base64 encoded]
>
> 204 No Content
