# File Object

A file served by Cellaro.

``` js
{
    "data": {
        "complete": true,
        "created_at": "2013-01-28T18:31:18Z",
        "derived_files": {
            "image_thumb_200s": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_200s.jpg"
            },
            "image_thumb_960r": {
                "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_960r.jpg"
            }
        },
        "id": "deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g",
        "image_info": {
            "height": 480,
            "width": 640
        },
        "kind": "image",
        "mime_type": "image/jpg",
        "name": "filename.jpg",
        "size": 34804,
        "sha1": "0fd0710c09c397bd63a731249f89e0ed0942476e",
        "url": "http://cdn1.example.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g.jpg"
    },
    "meta": {
        "code": 200
    }
}
```

## File Fields

<table>
    <thead>
        <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>annotations</code></td>
            <td>list</td>
            <td>[Optional] Metadata about the File. See the <a href="/annotations.md">Annotations</a> documentation.</td>
        </tr>
        <tr>
            <td><code>complete</code></td>
            <td>boolean</td>
            <td>Is this File considered complete? A File is complete once its contents are set. ( cached and derived files made available )</td>
        </tr>
        <tr>
            <td><code>created_at</code></td>
            <td>string</td>
            <td>Date/time at which this file was created. Counts when the initial call to create was made, not when the file was marked complete.</td>
        </tr>
        <tr>
            <td><code>derived_files</code></td>
            <td>object</td>
            <td>An object containing any other Files that were created using this File as input. Please see the section on <a href="#derived-files">Derived Files</a> for more information.</td>
        </tr>
        <tr>
            <td><code>file_token</code></td>
            <td>string</td>
            <td>[Optional] A token that can be used to modify a File. Only present upon creation time, if a write file token was specified when fetching this file or if you have the files scope. Please see the section on <a href="#file-authorization">File Authorization</a> for more information.</td>
        </tr>
        <tr>
            <td><code>id</code></td>
            <td>string</td>
            <td>Primary identifier for a File. Please see the section on <a href="/data-formats.md#object-ids">Object IDs</a> for more information</td>
        </tr>
        <tr>
            <td><code>image_info</code></td>
            <td>object</td>
            <td>[Optional] An object - available when <code>kind: 'image'</code>, containing the dimensions of the image file, expressed in pixels.</td>
        </tr>
        <tr>
            <td><code>kind</code></td>
            <td>string</td>
            <td>In broad terms, what kind of File is this? It can be user specified or it will be automatically guessed based on the File's <code>mime_type</code>. Must be one of: <code>image</code>, or <code>other</code>.</td>
        </tr>
        <tr>
            <td><code>mime_type</code></td>
            <td>string</td>
            <td>The user provided <a href="http://en.wikipedia.org/wiki/MIME">MIME type</a> of the file.</td>
        </tr>
        <tr>
            <td><code>name</code></td>
            <td>string</td>
            <td>[Optional] The User provided name of the File.</td>
        </tr>
        <tr>
            <td><code>sha1</code></td>
            <td>string</td>
            <td>[Optional] A <a href="http://en.wikipedia.org/wiki/SHA-1">SHA1</a> hash of the File contents.</td>
        </tr>
        <tr>
            <td><code>size</code></td>
            <td>int</td>
            <td>The number of bytes of the File's contents.</td>
        </tr>
        <tr>
            <td><code>url</code></td>
            <td>string</td>
            <td>The full URL to this File. It may return a 404 if the File has been deleted or if the URL has expired.</td>
        </tr>
        <tr>
            <td><code>url_expires</code></td>
            <td>string</td>
            <td>[Optional] A <a href="/data-formats.md#dates">date and time</a> indicating when the provided <code>url</code> will no longer be valid. If the expiration has passed, please refetch the File or underlying object to get a new URL to use.</td>
        </tr>
    </tbody>
</table>

## Derived Files

When a file is handled by Cellaro, it may choose to create other files based on the original. A User cannot upload their own derived files. Derived files will include the keys shown in the example below. Please see [the File Fields documentation](#file-fields) for an explanation of each key.

``` js
"image_thumb_200s": {
    "mime_type": "image/png",
    "sha1": "be91cb06d69df13bb103a359ce70cf9fba31234a",
    "size": 33803,
    "url": "http://cdn1.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_thumb_200s.jpg"
}
```

The current valid derived files are:

* `image_thumb_200s`: When the root file is an image, Cellaro will generate a 200x200 square thumbnail of the image that shrinks and crops the center square of the image.
* `image_thumb_960r`: When the root file is an image, Cellaro will scale down the entire image so it fits within a 640x960 pixel bounding box. This thumbnail will not be cropped.

Incomplete Files ( `complete: false` ), hold a temporary Derived File URL as shown below:

```
"image_thumb_200s": {
    …other data…
    "url": "http://derive.cellaroserver.com/deb3d1e9-bd1b-4b1e-a8f2-aba7c982ff2g_200s"
}
```

The `url` points to the on-demand Derived Files server. Clients shouldn't rely on this URL and refresh the File as soon as possible to ensure they're using the cached, CDN'd, static File (that should be shared and used by the Derived Files server as well once available).

## File Authorization

File tokens are included with file objects as `file_token` and are used in conjunction with /api/v0/files endpoints by passing a `file_token` query string parameter (regardless of the HTTP method used).

Tokens are included in File objects:

* upon creation
* if a file is retrieved from a file endpoint AND a writeable file_token query string parameter is passed in.

Write tokens are NEVER returned in Annotations. The Streaming API does not include file tokens of any kind.

## General Parameters

Requests that return streams of Files respond to [pagination parameters](/pagination.md). Additionally they accept the following query string parameters:

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Required?</th>
            <th width="50">Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>file_types</code></td>
            <td>Optional</td>
            <td>string</td>
            <td>A comma separated list of the File types to include. For instance <code>file_types=net.myapp</code> will only return files with a type of <code>net.myapp</code>.</td>
        </tr>
        <tr>
            <td><code>include_incomplete</code></td>
            <td>Optional</td>
            <td>integer (0 or 1)</td>
            <td>Should incomplete files be included in the result? (Default: <code>True</code>)</td>
        </tr>
        <tr>
            <td><code>include_annotations</code></td>
            <td>Optional</td>
            <td>integer (0 or 1)</td>
            <td>Should <a href="/annotations.md">annotations</a> be included in the response objects? Defaults to false.</td>
        </tr>
        <tr>
            <td><code>include_file_annotations</code></td>
            <td>Optional</td>
            <td>integer (0 or 1)</td>
            <td>Should <a href="/annotations.md">File annotations</a> be included in the response objects? Defaults to false.</td>
        </tr>
    </tbody>
</table>

## Attaching Files to other resources

A Cellaro file can be attached to any resource that allows annotations. You can attach a file to a resource with any annotation by using the [annotation replacement values](/annotations.md#annotation-replacement-values). For instance, to attach a photo that you've uploaded as a File to a new Explore Timeline Event, you would send the following annotations:

``` js
[
    {
        "type": "com.cellaroserver.core.oembed",
        "value": {
            "+com.cellaroserver.core.file": {
                "file_id": "1",
                "file_token": "the write file_token you received when you uploaded your file",
                "format": "oembed"
            }
        }
    }
]
```

Then, when your content is returned through the API, Cellaro will replace that annotation with an OEmbed annotation that represents the referenced file:

``` js
[
    {
        "type": "com.cellaroserver.core.oembed",
        "value": {
            "file_id": "1",
            "url": "<a short-lived URL to the file content>",
            "url_expires": "2018-01-01T00:00:00Z",
            "type": "photo",
            "version": "1.0",
            ...remaining OEmbed data...
        }
    }
]
```

Each file must be specified as an object with a `format` property. Each object is transformed into another object containing `file_id`, and any additional data as specified by the `format` key. If a file is not found or you don't have permission to access it, the `file_id` value returned may not exist (or may not be an integer).

The current valid formats:

* `metadata`: This includes the entire File resource except for the `annotations`, `source`, and `user` fields.
* `oembed`: This includes any OEmbed data we can generate for this file. This could be empty. This format can only be used with the `com.cellaroserver.core.oembed` annotation.
* `url`: The includes just a url pointing to this file's contents.

Please see the [File replacement annotation value](/annotation-replacement-values/+com.cellaroserver.core.file.md) for more details and examples.
