# Streaming API

The Events Web Socket endpoint provides real time access to Stream data for any given Resource and Context.

## URL

    https://stream.cellaroserver.com/v0/:resource

The connection should be automatically upgraded to wss://. Web Sockets over SSL are used in order to prevent network issues caused by proxies.

Web Socket frame data uses api-wide Response Envelope.

<table>
    <tr>
        <th>Resource</th>
        <th>Contexts</th>
    </tr>
    <tr>
        <td><code>discover</code></td>
        <td><code>program</code></td>
    </tr>
</table>

## Connection

The streams interface allows a client to request Context data from a Resource.

### Example

Connecting to the `discover` Resource using socket.io:

    io.connect( 'https://stream.cellaroserver.com/v0/:resource' )

## Context Subscription

The following events allow a client to manage Context subscriptions.

### Events

<table>
    <tr>
        <th>Field</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>context:join</code></td>
        <td>Joins the context identified by the subscription data, emitted by the client.</td>
    </tr>
    <tr>
        <td><code>context:leave</code></td>
        <td>Joins the context identified by the subscription data, emitted by the client.</td>
    </tr>
    <tr>
        <td><code>context:list</code></td>
        <td>Returns a list of currently subscribed contexts, identified by their subscription data. Can be emitted by the client, or the server as join/leave confirmation.</td>
    </tr>
</table>

### Available Contexts

#### Program

Available in the `discover` resource, provides discover data for a given Program.

##### Program subscription data

<table>
    <tr>
        <th>Field</th>
        <th>Required?</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>channel_id</code></td>
        <td>Required</td>
        <td>string</td>
        <td>Channel Object ID, 42 characters, may only contain A-Z, 0-9 and dashes.</td>
    </tr>
    <tr>
        <td><code>program_id</code></td>
        <td>Required</td>
        <td>string</td>
        <td>Program Object ID, 42 characters, may only contain A-Z, 0-9 and dashes.</td>
    </tr>
    <tr>
        <td><code>dedicated</code></td>
        <td>Optional</td>
        <td>boolean</td>
        <td>True for a dedicated context, not shared with any other client. Useful when previewing timeline-based content i.e. a curated program.</td>
    </tr>
</table>

###### Notes

Both `program_id` and `channel_id` are required to prevent context ambiguity  as a `program_id` might be valid for more than one channel e.g. an infomercial.

###### Example

    send    : 'context:join', { program_id: 48, channel_id: 12 }
    receive : 'context:list', [ context_subscription_data, … ]

##### Events

Events use the api-wide Response Envelope.

##### Event: Data event ( `data` )

Data for a given Context. The Response Envelope contains the context in its `meta` property. The `data` property will, for example, contain an [Event object](/objects.md).

###### Example

    {
        "meta": {
            "code": 200
            "context": context_subscription_data
        },
        "data": [
            {
                "Event object": "could go here"
            }
        ]
    }

##### Event: Dead Air ( `dead_air` )

Sent when there's no data associated with the show. Response envelope meta has a 204 status code and message.

<table>
    <tr>
        <th>Status</th>
        <th>Message</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>204</td>
        <td>nlp delay</td>
        <td>Natural Language Processing is silent</td>
    </tr>
    <tr>
        <td>204</td>
        <td>mms delay</td>
        <td>Media Monitoring Service is silent</td>
    </tr>
    <tr>
        <td>204</td>
        <td>search delay or no content</td>
        <td>self explanatory</td>
    </tr>
</table>

###### Example

    {
        "meta": {
            "code": 204,
            "context": context_subscription_data
        },
        "data": []
    }

##### Event: Control - Clear ( `control:clear` )

Server-issued control event, forces the client to clear current event buffer display.

###### Example

    {
        "meta": {
            "code": 205,
            "context": context_subscription_data
        },
        "data": []
    }
