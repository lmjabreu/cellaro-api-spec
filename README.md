# Cellaro API Spec

This repository holds the **recommended API Spec** for Cellaro.

It is intended to describe a simple, clean, scalable API for consumption by the Sommelier Client.

Objects are made as simple as possible, but not too simple. Object re-use is promoted as a way to maintain consistency and predictability between common data structures.

Removal of superfluous keys, alphabetical ordering of properties allows for better compression of data, simpler consumption.

## Very Important Warning

**This is a living, working document.**

**Remember**: the API is designed to be of easy consumption, it is **not** a direct access to the database or source data.

## Latest updates

* (Apr 24, 2013) [Added, Updated] RESTful and Streaming apis have their own subdomain, Added suppress_response_code, Added partial responses
* (Apr 15, 2013) [Updated] API version in url now includes a 'v' prefix
* (Mar 28, 2013) [Added] Dimensions in File Object when it's of the `image` kind
* (Mar 23, 2013) [Added] Entities
* (Mar 22, 2013) [Added] Offer, Airing, Program Annotations
* (Mar 21, 2013) [Added] Language and Currency data formats.
* (Mar 13, 2013) [Added] displaySize to Event object
* (Mar 11, 2013) [Added] Files
* (Mar 11, 2013) [Updated] Schedule endpoint with `showcased` parameter
* (Mar 07, 2013) [Added] Annotation Replacement Values
* (Mar 06, 2013) [Added] Annotations
* (Mar 05, 2013) [Added] Responses, Pagination, Data Formats
* (Feb 11, 2013) [Added] Streaming API

## Table of Contents

* [Object definitions](/objects.md) - e.g.: airing, program, event, …
* [Resource definitions](/resources) - e.g.: /schedule, /events, …
* [Responses](/responses.md) - details such as response envelope, format, …
* [Annotations](/annotations.md) - metadata added to Objects
* [Data formats](/data-formats.md) - Dates, language, currency, …
* [Pagination](/pagination.md)

## General Notes

### Versioning

The API is url versioned, meaning every endpoint is prefixed with the API version number.

> http://api.cellaroserver.com/v0/:endpoint

The current version of the API is version 0. Deprecated API versions shall return a '410 - Gone' HTTP status code. Support is limited to the current and previous api versions.

### UTF-8 encoding

Every string passed to and from the Cellaro API needs to be UTF-8 encoded. For maximum compatibility, normalize to Unicode Normalization Form C (NFC) before UTF-8 encoding.

### Abstraction

Specific data sources, such as Gracenote - used for Schedule data, are abstracted from the URL and Schema for flexibility and future-proof the API.

### Performance

The API responses should be GZipped and cached whenever possible. The number of requests required for a Client to fetch the required information should be minimized, whenever possible give preference to verbosity rather than a reference model.

#### Caching

Media objects are cached on Cellaro to make sure data is always available after curation. This also allows us to optimize the data return, caching, pre-burn common image sizes.

## Maintainers

* [Luis Abreu](mailto:luis@ribot.co.uk)
