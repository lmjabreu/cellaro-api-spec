# Objects

* [Airing](#airing)
* [Channel](#channel)
* [Program](#program)
* [Event](#event)
* [File](#file)

## Airing

An airing defines when a certain Program will air on a specific Channel.

It contains references to the Channel and Program, using their Unique IDs, and start and end keys containing air times expressed in ISO 8601 UTC format.

### Example Airing object

``` js
{
  "channel_id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB",
  "end": "2012-09-04T10:00:00Z",
  "program_id": "270492450-003C505A9FF2EB56D483276A36D45C96",
  "start": "2012-09-04T08:00:00Z"
}
```

### Airing fields

<table>
    <tr>
        <th>Field</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>channel_id</code></td>
        <td>string</td>
        <td>Channel Object ID, 42 characters, may only contain A-Z, 0-9 and dashes.</td>
    </tr>
    <tr>
        <td><code>end</code></td>
        <td>string</td>
        <td>The time at which the Program will end airing in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601</a> format.</td>
    </tr>
    <tr>
        <td><code>program_id</code></td>
        <td>string</td>
        <td>Program Object ID, 42 characters, may only contain A-Z, 0-9 and dashes.</td>
    </tr>
    <tr>
        <td><code>start</code></td>
        <td>string</td>
        <td>The time at which the Program will start airing in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601</a> format.</td>
    </tr>
</table>

#### Deprecations

* none

## Channel

A Channel contains basic information about a TV Channel.

``` js
{
    "annotations": [
        {
            "type": "com.cellaroserver.core.oembed",
            "value": {
                "the real annotation data": "would go here"
            }
        }
    ],
    "id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB",
    "name": "Cable News Network",
    "short_name": "CNN"
}
```

### Channel Fields

<table>
    <tr>
        <th>Field</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>id</code></td>
        <td>string</td>
        <td>Channel Object ID, 42 characters, may only contain A-Z, 0-9 and dashes.</td>
    </tr>
    <tr>
        <td><code>annotations</code></td>
        <td>list</td>
        <td>Metadata about the Channel. See the <a href="/annotations.md">Annotations</a> documentation.</td>
    </tr>
    <tr>
        <td><code>name</code></td>
        <td>string</td>
        <td>The long name of the Channel</td>
    </tr>
    <tr>
        <td><code>short_name</code></td>
        <td>string</td>
        <td>Abbreviated version of the Channel name.</td>
    </tr>
</table>

#### Deprecations

* none

## Program

A Program contains basic information about a tv program such as: `id`, `showcased`, `synopsis`, `title`.

> More information such as `category`, `rating` and interest data such as `likes` or `stats` might be added in the future.

``` js
{
    "annotations": [
        {
            "type": "com.cellaroserver.core.oembed",
            "value": {
                "the real annotation data": "would go here"
            }
        }
    ],
    "id": "270492450-003C505A9FF2EB56D483276A36D45C96",
    "showcased": true,
    "synopsis": "New York City policeman John McClane (Bruce Willis) is visiting his estranged wife (Bonnie Bedelia) and two daughters on Christmas Eve. He joins her at a holiday party in the headquarters of the Japanese-owned business she works for. But the festivities are interrupted by a group of terrorists who take over the exclusive high-rise, and everyone in it. Very soon McClane realizes that there's no one to save the hostages -- but him.",
    "title": "Die Hard"
}
```

### Program Fields

<table>
    <thead>
        <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>annotations</code></td>
            <td>list</td>
            <td>Annotation Object containing data associated with this Program. <br><br>Typically an oEmbed annotation. <br><br>See <a href="/annotations.md">annotations documentation</a>.</td>
        </tr>
        <tr>
            <td><code>id</code></td>
            <td>string</td>
            <td>Channel Object ID, 42 characters, may only contain a-z, 0-9 and dashes.</td>
        </tr>
        <tr>
            <td><code>showcased</code></td>
            <td>boolean</td>
            <td>Wether a Program is showcased</td>
        </tr>
        <tr>
            <td><code>synopsis</code></td>
            <td>string</td>
            <td>This Program's description. May contain HTML.</td>
        </tr>
        <tr>
            <td><code>title</code></td>
            <td>list</td>
            <td>The Program title.</td>
        </tr>
    </tbody>
</table>

#### Deprecations

* none

## Event

Generic event container that includes in its `annotations` object a list of relevant data for a specific event. An event may contain only one concept but multiple oEmbed annotations.

``` js
{
    "annotations": [
        {
            "type": "com.cellaroserver.core.concept",
            "value": {
                "the real annotation data": "would go here"
            }
        },
        {
            "type": "com.cellaroserver.core.oembed",
            "value": {
                "the real annotation data": "would go here"
            }
        }
    ],
    "end": "00:00:09.000",
    "start": "00:00:04.000"
}
```

### Event Fields

<table>
    <thead>
        <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>annotations</code></td>
            <td>list</td>
            <td>A list of Annotation objects associated with this Event. See the <a href="/annotations.md">annotations documentation</a>.</td>
        </tr>
        <tr>
            <td><code>end</code></td>
            <td>integer</td>
            <td>Timestamp that marks the end of the Event.</td>
        </tr>
        <tr>
            <td><code>start</code></td>
            <td>integer</td>
            <td>Timestamp that marks the start of the Event.</td>
        </tr>
    </tbody>
</table>

> **Note**: the `annotations` `value` property will be augmented with a `displaySize` property, documented below.

<table>
    <thead>
        <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>displaySize</code></td>
            <td>string</td>
            <td>
              Indicates the intended display size as specified by the user. Suggested presentation sizes (px) are:
              <table>
                <thead>
                  <tr>
                    <th>Value</th>
                    <th>Size (width,height)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>small</td>
                    <td>270x140</td>
                  </tr>
                  <tr>
                    <td>medium</td>
                    <td>270x360</td>
                  </tr>
                  <tr>
                    <td>large</td>
                    <td>570x360</td>
                  </tr>
                  <tr>
                    <td>fullscreen</td>
                    <td>870x650</td>
                  </tr>
                </tbody>
              </table>
            </td>
        </tr>
    </tbody>
</table>

#### Deprecations

* none

## File

[Documented here](/resources/file/file.md).
