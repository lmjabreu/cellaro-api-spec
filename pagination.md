# Pagination

Most Cellaro API endpoints that return lists of objects are paginated. Understanding how paginated data is requested and returned is key to retrieving data efficiently from the API.

## Parameters

Requests for paginated streams can be filtered by passing the following query string parameters along with the request:

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Required?</th>
            <th width="50">Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>since</code></td>
            <td>Optional</td>
            <td>string</td>
            <td>Return objects following the <code>max</code> provided in the <a href="#response-metadata">response metadata</a> of a previous query.</td>
        </tr>
        <tr>
            <td><code>before</code></td>
            <td>Optional</td>
            <td>string</td>
            <td>Return objects preceding the <code>min</code> provided in the <a href="#response-metadata">response metadata</a> of a previous query.</td>
        </tr>
        <tr>
            <td><code>count</code></td>
            <td>Optional</td>
            <td>integer</td>
            <td>The number of objects to return, up to a maximum of 200.</td>
        </tr>
    </tbody>
</table>

## Response Metadata

Responses from paginated streams will include the following fields in the `meta` object of the [response envelope](/responses.md#response-envelope).

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>max</code></td>
            <td>string</td>
            <td>The latest Date returned in the data set. Inclusive. This should be considered an opaque identifier that may be passed as <code>since</code> in the next call in order to retrieve the next set of objects.</td>
        </tr>
        <tr>
            <td><code>min</code></td>
            <td>string</td>
            <td>The earliest Date returned in the data set. Inclusive. This should be considered an opaque identifier that may be passed as <code>before</code> in the next call in order to retrieve the previous set of objects.</td>
        </tr>
        <tr>
            <td><code>more</code></td>
            <td>boolean</td>
            <td>If <code>more</code> is <code>true</code>, there are more matches available for the given query than would fit within <code>count</code> objects. If <code>more</code> is <code>false</code>, there are no more matches available.</td>
        </tr>
    </tbody>
</table>