# File

<!-- specify the "key" for the replacement value -->
> ### +com.cellaroserver.core.file

<!-- provide a description of the replacement value -->
This dynamically inserts information about a Cellaro [File](/resources/file/) into an annotation. The File information is merged with any other values to form a single object.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

### `url` format

#### Provided to Cellaro

``` js
{
    "type": "com.example.test",
    "value": {
        "+com.cellaroserver.core.file": {
            "file_token": "12345abcde",
            "format": "url",
            "file_id": "1"
        },
        "other_values": "are preserved"
    }
}
```

#### Returned by API

``` js
{
    "type": "com.example.test",
    "value": {
        "file_id": "1",
        "url": "http://example.com/file_url.png",
        "url_expires": "2013-01-25T03:00:00Z",
        "other_values": "are preserved"
    }
}
```


### `metadata` format

#### Provided to Cellaro

``` js
{
    "type": "com.example.test",
    "value": {
        "+com.cellaroserver.core.file": {
            "file_token": "12345abcde",
            "format": "metadata",
            "file_id": "1"
        },
        "other_values": "are preserved"
    }
}
```

#### Returned by API

``` js
{
    "type": "com.example.test",
    "value": {
        "file_id": "1",
        "complete": true,
        "mime_type": "image/png",
        ...other File object values...
        "other_values": "are preserved"
    }
}
```


### `oembed` format

This format can only be used with the `com.cellaroserver.core.oembed` annotation, not with 3rd party annotations.

#### Provided to Cellaro

``` js
{
    "type": "com.cellaroserver.core.oembed",
    "value": {
        "+com.cellaroserver.core.file": {
            "file_token": "12345abcde",
            "format": "oembed",
            "file_id": "1"
        },
        "other_values": "are preserved"
    }
}
```

#### Returned by API

``` js
{
    "type": "com.example.test",
    "value": {
        "file_id": "1",
        "url": "https://...",
        "url_expires": "2012-01-01T01:00:00Z",
        "version": "1.0",
        "type": "photo",
        ...other oembed values...
        "other_values": "are preserved"
    }
}
```

The `url` will provide the current value for the file. (This expires at `url_expires`). `thumbnail_url` and `thumbnail_large_url` (described below) also use this same url patterns.

We generate `thumbnail_url` from the [`image_thumb_200s` derived file](/resources/file/file.md#derived-files). If the image is smaller than 200x200, then we use the image as its own thumbnail. If we have a larger derived file (`image_thumb_960r`), then we will also include `thumbnail_large_url`, `thumbnail_large_width`, `thumbnail_large_height`, etc that correspond to the larger thumbnail.

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

| Field | Required? | Type | Description |
| ----- | --------- | ---- | ----------- |
| `file_token` | Required | string | A valid file token that Cellaro returned when you uploaded a file.|
| `format` | Required | string | Either `metadata`, `oembed`, or `url` depending on what data you want provided for this file. |
| `file_id` | Required | string | The id of the file. |

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)
* [Sous](http://sous.nbwd.co.uk/)

<!-- provide references to related annotations -->
## Related annotations
* [com.cellaroserver.core.oembed](/annotations/com.cellaroserver.core.oembed.md)