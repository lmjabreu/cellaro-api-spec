# File List

<!-- specify the "key" for the replacement value -->
> ### +com.cellaroserver.core.file_list

<!-- provide a description of the replacement value -->
This dynamically inserts information about a list of Cellaro [Files](/resources/file/) into an annotation.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

### Provided to Cellaro
``` js
{
    "type": "com.example.test",
    "value": {
        "+com.cellaroserver.core.file_list": [
            {
                "file_token": "12345abcde",
                "format": "url",
                "file_id": "1"
            },
            {
                "file_token": "98765zyx",
                "format": "url",
                "file_id": "1"
            }
        ],
        "other_values": "are preserved"
    }
}
```

### Returned by API
``` js
{
    "type": "com.example.test",
    "value": {
        "com.cellaroserver.core.file_list": [
            {
                "file_token_read": "read file_token",
                "file_id": "1"
                "url": "http://example.com/file1.png",
                "url_expires": "2013-01-25T03:00:00Z"
            },
            {
                "file_token_read": "read file_token",
                "file_id": "2"
                "url": "http://example.com/file2.png",
                "url_expires": "2013-01-25T03:00:00Z"
            }
        ],
        "other_values": "are preserved"
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

Each element in the `+com.cellaroserver.core.file_list` list must match the format for [+com.cellaroserver.core.file](/annotation-replacement-values/+com.cellaroserver.core.file.md)

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)
* [Sous](http://sous.nbwd.co.uk/)

<!-- provide references to related annotations -->
## Related annotations
* [+com.cellaroserver.core.file](/annotation-replacement-values/com.cellaroserver.core.file.md)