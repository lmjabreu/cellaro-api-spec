<!-- give your annotation a title -->
### Offer

<!-- specify the "type" for your annotation -->
> ### com.cellaroserver.core.offer

<!-- provide a description of what your annotation represents -->
The offer annotation allows a User to specify pricing information for a specific content.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

``` js
{
    "type": "com.cellaroserver.core.offer",
    "value": {
        "price": 10,
        "price_currency": "GBP"
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

| Field | Required? | Type | Description |
| ----- | --------- | ---- | ----------- |
| `price` | Required | number | The price value for this offer. For free products use `0` instead of ommiting the property. A dot `.` is used to mark the decimal number. |
| `priceCurrency` | Required | string | A valid [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217) currency code. |

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)

<!-- provide references to related annotations -->
## Related annotations
