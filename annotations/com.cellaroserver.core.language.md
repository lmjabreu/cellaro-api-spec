<!-- give your annotation a title -->
### Language

<!-- specify the "type" for your annotation -->
> ### com.cellaroserver.core.language

<!-- provide a description of what your annotation represents -->
The language annotation allows a User to indicate what language this content was written in.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

``` js
{
    "type": "com.cellaroserver.core.language",
    "value": {
        "language": "en"
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

| Field | Required? | Type | Description |
| ----- | --------- | ---- | ----------- |
| `price` | Required | number | A valid [ISO 639-1](http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) language code. Note that we only accept a subset of language codes right now. |

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)
* [Sous](http://sous.nbwd.co.uk/)

<!-- provide references to related annotations -->
## Related annotations
