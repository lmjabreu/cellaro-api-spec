<!-- give your annotation a title -->
### Geolocation

<!-- specify the "type" for your annotation -->
> ### com.cellaroserver.core.geolocation

<!-- provide a description of what your annotation represents -->
The geolocation annotation is meant to specify a geographic point on Earth. It is not meant to specify:

* a human place (city, building, park, "San Francisco", "The Mission", "The Moscone Center").
* paths, regions, or complex geographic shapes. We recommend using a common schema (like [GeoJSON](http://www.geojson.org/)) in your own annotation if you need this kind of solution.

<!-- provide at least one example of what your annotation might look like in the wild -->
#### Examples

Just the required parameters:
``` js
{
    "type": "geolocation",
    "value": {
        "latitude": 50.827843,
        "longitude": -0.128746
    }
}
```

With all optional parameters:
``` js
{
    "type": "geolocation",
    "value": {
        "latitude": 50.827843,
        "longitude": -0.128746,
        "altitude": 38.195,
        "horizontal_accuracy": 100,
        "vertical_accuracy": 100
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
#### Fields

<table>
    <tr>
        <th>Field</th>
        <th>Required?</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>latitude</code></td>
        <td>Required</td>
        <td>decimal</td>
        <td>The latitude of the geographic location in decimal degrees. Must range from -90 (the South Pole) to 90 (the North Pole).</td>
    </tr>
    <tr>
        <td><code>longitude</code></td>
        <td>Required</td>
        <td>decimal</td>
        <td>The longitude of the geographic location in decimal degrees. Must range from -180 to 180.</td>
    </tr>
    <tr>
        <td><code>altitude</code></td>
        <td>Optional</td>
        <td>decimal</td>
        <td>The altitude (in meters) of the geographic location. Can be negative.</td>
    </tr>
    <tr>
        <td><code>horizontal_accuracy</code></td>
        <td>Optional</td>
        <td>decimal</td>
        <td>The horizontal accuracy (in meters) of the instrument providing this geolocation point. Must be >= 0.</td>
    </tr>
    <tr>
        <td><code>vertical_accuracy</code></td>
        <td>Optional</td>
        <td>decimal</td>
        <td>The vertical accuracy (in meters) of the instrument providing this geolocation point. Must be >= 0.</td>
    </tr>
</table>

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)
* [Sous](http://sous.nbwd.co.uk/)

<!-- provide references to related annotations -->
## Related annotations