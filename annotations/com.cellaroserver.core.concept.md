<!-- give your annotation a title -->
# Concept

<!-- specify the "type" for your annotation -->
> ### com.sommelierserver.core.concept

<!-- provide a description of what your annotation represents -->
The concept annotation is meant to specify information extracted from Closed Captions using Natural Language Processing.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

``` js
{
    "type": "com.sommelierserver.core.concept",
    "value": {
        "id": "Dark ride-00:00:12.720",
        "name": "Dark Ride",
        "probability": 0.7310585786300049
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

| Field       | Required? | Type   | Description                                                             |
| -----       | --------- | ----   | -----------                                                             |
| id          | Required  | string | Concept Object ID, 36 characters, may only contain a-z, 0-9 and dashes. |
| name        | Required  | string | Name of the Concept.                                                    |
| probability | Required  | float  | Level of confidence attributed by the Natural Language Processing algorithm. Ranges from 0 to 1, the latter being the highest confidence level.<br> The higher the value, the higher the probability of it being related to the Program content. |

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)
* [Sous](http://sous.nbwd.co.uk/)

<!-- provide references to related annotations -->
## Related annotations