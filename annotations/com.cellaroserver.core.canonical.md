<!-- give your annotation a title -->
# Canonical

<!-- specify the "type" for your annotation -->
> ### com.sommelierserver.core.canonical

<!-- provide a description of what your annotation represents -->
The canonical annotation is meant to specify the original or canonical source of content from somewhere else on the web.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

``` js
{
    "type": "com.sommelierserver.core.canonical",
    "value": {
        "url": "http://www.flickr.com/photos/21471918@N05/8528878160"
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

| Field | Required? | Type   | Description                                                 |
| ----- | --------- | ----   | -----------                                                 |
| url   | Required  | string | A valid URL pointing to the source of the original content. |

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)
* [Sous](http://sous.nbwd.co.uk/)

<!-- provide references to related annotations -->
## Related annotations