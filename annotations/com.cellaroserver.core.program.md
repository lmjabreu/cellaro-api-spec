<!-- give your annotation a title -->
### Program

<!-- specify the "type" for your annotation -->
> ### com.cellaroserver.core.program

<!-- provide a description of what your annotation represents -->
The Program annotation allows a User to attach a Program Object to content.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

``` js
{
    "type": "com.cellaroserver.core.program",
    "value": {
        "id": "270492450-003C505A9FF2EB56D483276A36D45C96",
        "title": "Die Hard",
        "other fields": "go here"
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

See the [Program Object](/objects.md#program) for more information.

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)

<!-- provide references to related annotations -->
## Related annotations
