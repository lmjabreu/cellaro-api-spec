<!-- give your annotation a title -->
### Airing

<!-- specify the "type" for your annotation -->
> ### com.cellaroserver.core.airing

<!-- provide a description of what your annotation represents -->
The Airing annotation allows a User to attach an Airing Object to content.

<!-- provide at least one example of what your annotation might look like in the wild -->
## Example

``` js
{
    "type": "com.cellaroserver.core.airing",
    "value": {
      "channel_id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB",
      "end": "2012-09-04T10:00:00Z",
      "program_id": "270492450-003C505A9FF2EB56D483276A36D45C96",
      "start": "2012-09-04T08:00:00Z"
    }
}
```

<!-- provide a complete description of the fields in the "value" object for your annotation -->
## Fields

See the [Airing Object](/objects.md#airing) for more information.

<!-- provide a way to contact you -->
## Maintainers
* Luis Abreu ( [luis@ribot.co.uk](mailto:luis@ribot.co.uk) )

<!-- provide references to compatible apps / service -->
## Used by
* [Sommelier Client](http://production.sommelierclient.com/)

<!-- provide references to related annotations -->
## Related annotations
