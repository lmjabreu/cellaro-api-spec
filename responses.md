# Responses

All responses to requests to the Cellaro API endpoints listed under [Resources](/resources/), whether successful or not, will be returned in the same type of envelope structure. This document describes how that envelope works and what it may contain.

## Response Envelope

The top-level response is an object containing two keys. The first key, `data`, corresponds to the actual response item requested. This may either be an object itself or a list of objects. The particular data returned is described in each endpoint's documentation. If the request is unsuccessful (results in an error), no `data` key will be present.

The second key present, `meta`, corresponds to an object containing additional information about the request. This object will always contain `code`, a copy of the HTTP status code that has been returned. It will also contain [pagination metadata](/pagination#response-metadata). Note that a `message` property might be present, see ["HTTP Status Code Suppression"](#http-status-code-suppression) for an example.

### Sample Response Envelope

``` js
{
    "data": {
        ...
    },
    "meta": {
        "code": 200,
        "max": "2012-12-31T13:22:55Z",
        "min": "2012-12-31T16:22:55Z",
        "more": true
    }
}
```

## HTTP Status Code Suppression

The REST api supports status code suppression for clients that handle non 200 status codes incorrectly.

For example:

    https://api.cellaroserver.com/v0/schedule?count=2000000&suppress_response_codes=true

Will result in a response that looks something like this:

    HTTP status code: 200 OK
    {
      "meta": {
        "code": 400,
        "message": "Bad Request - count value exceeds max value. use 1-200."
      }
    }

## Partial Response

A partial response may be requested by providing the desired fields via a url argument. This is useful when the client is only interested in a specific set of fields and fetching the full content would be detrimental to performance.

For example: a client might request schedule information but only be interested in getting a list of programs with their `id and `showcased` flag.

    https://api.cellaroserver.com/v0/schedule?fields=airings,channels(id),programs(id,showcased)

Will result in a response that looks something like this:

``` js
{
    "data": {
      "airings": [
        {
          "channel_id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB",
          "end": "2012-12-31T12:30:00Z",
          "program_id": "270492450-003C505A9FF2EB56D483276A36D45C96",
          "start": "2012-12-31T10:30:00Z"
        }
      ],
      "channels": [
        {
          "id": "251533197-CE3BEA67AA7ADB52D50206C44F9FA5DB"
        }
      ],
      "programs": [
        {
            "id": "270492450-003C505A9FF2EB56D483276A36D45C96",
            "showcased": true
        }
      ]
    },
    "meta": {
        "code": 200,
        "max": "2012-12-31T09:22:55Z",
        "min": "2012-12-31T15:22:55Z",
        "more": true
    }
}
```

The `fields` parameter accepts simple nested selectors.

Root parameter filtering

?fields=airings

?fields=airings,programs/id

?fields=airings,programs(id,showcased)

?fields=airings,programs(id,annotations/type)

## JSONP

We support JSONP for easy, unauthenticated cross-domain API requests with wide browser support. Normal JSON responses are wrapped in a Javascript function so they may be included in a webpage and fetched directly by the browser via a `script` tag. It is not possible to make requests to API endpoints which require authentication with JSONP.

To use JSONP, add a `callback` parameter to the request's query string. For example:

    https://api.cellaroserver.com/v0/schedule?callback=awesome

Will result in a response that looks something like this:

    awesome({...})

When using JSONP, our servers will return a 200 status code in the HTTP response, regardless of the effective status code.

For more information on JSONP, see the Wikipedia page for [JSONP](http://en.wikipedia.org/wiki/JSONP).

## CORS

We support CORS for authenticated cross-domain API requests direct from browsers. Support for CORS may vary by browser. When using CORS, you are still responsible for obtaining, storing and supplying a valid access token with each request, if access to authenticated endpoints is required. For more information on CORS, see the Wikipedia page for [CORS](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing).
