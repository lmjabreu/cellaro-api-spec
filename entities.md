# Entities

* [Programs](#programs)
* [Mentions](#mentions)
* [Links](#links)
* [User Specified Entities](#user-specified-entities)

Entities provide common formatting for programs and mentions but they also allow links to be embedded with anchor text which gives more context. Each entity type is a list with 0 or more entities of the same type.

Usually entities are extracted from the text property by Cellaro servers. We allow users to specify some entities at Object creation time. Please refer to the [user specified entities](#user-specified-entities) documentation for more information.

Ranges specified by entities may be adjacent, but may not overlap.

**Note:** `pos` and `len` fields are given as UTF-32 code points. Many string implementations (in particular, Cocoa's NSString class and Javascript's strings) use UTF-16 or UCS-2 encoding internally, and thus the indices given will not map directly to UTF-16 code points if encoded with surrogate pairs, e.g., emoji characters.

## Programs

References a Program.

> Because you and Rifa both watched Bob's Burgers

``` js
"programs": [
  {
    "name": "Bob's Burgers",
    "id": 1,
    "pos": 34,
    "len": 13
  }
]
```

| Field | Type | Description |
| ----- | ---- | ----------- |
| `name` | string | The program being mentioned. |
| `id` | string | The program id of the mentioned program. |
| `pos` | integer | The 0 based index where this entity begins `text`. |
| `len` | integer | The length of the substring that represents this program. |

## Mentions

References a User.

> Because you and Rifa both watched Bob's Burgers

``` js
"mentions": [
  {
    "name": "Rifa",
    "id": "2",
    "pos": 16,
    "len": 4,
  }
]
```

| Field | Type | Description |
| ----- | ---- | ----------- |
| `name` | string | The user being mentioned. |
| `id` | string | The user id of the mentioned user. |
| `pos` | integer | The 0 based index where this entity begins `text`. |
| `len` | integer | The length of the substring that represents this mention. |

## Links

Link to another website.

> Wikis serve many different purposes, such as knowledge management and notetaking.

``` js
"links": [
  {
    "text": "knowledge management",
    "url": "http://en.wikipedia.org/wiki/Knowledge_management",
    "pos": 45,
    "len": 49,
    "amended_len": 84
  },
  {
    "text": "notetaking",
    "url": "http://en.wikipedia.org/wiki/Notetaking_software",
    "pos": 70,
    "len": 48,
    "amended_len": 73
  }
]
```


| Field | Type | Description |
| ----- | ---- | ----------- |
| `text` | string | The anchor text to be linked (could be a url). |
| `url` | string | The destination url (only http or https accepted) |
| `pos` | integer | The 0 based index where this entity begins `text`. |
| `len` | integer | The length of the substring in `text` that represents this link. |
| `amended_len` | integer | The length of the substring in `text` that represents this link including any [phishing protection](#links-with-custom-anchor-text) that was inserted by Cellaro. This can be used to customize the display of the anti-phishing information we provide. If this link has no anti-phishing protection (because the domain of the `url` matches the `text`), then this field will be omitted. |

### URI Templates

Links can contain [URI templates](http://tools.ietf.org/html/rfc6570) that the server will process. This allows you to create a link to the object you are creating. For instance, when you attach an image (oEmbed) to an Event. Cellaro could add a url to the end of the Event text that contains:

> cdn.cellaroserver.com/images/{event_id}

and the server replaces `{event_id}` with the id of the Event once it has been saved. By default, this processing happens on all links (both custom links and links extracted by the API). If you would like a link to not be processed, you can create the link as a [custom link](#links-with-custom-anchor-text) and include `"process_template": false` in the JSON for the link.

#### Replacement variables

Currently, the only variables for URI templates that we define are ones that could not be known when the link is being constructed. Additionally, if you specify a variable that is not listed below, we will pass that through instead of replacing it with nothing.

| Value | Description |
| ----- | ----------- |
| `event_id` | The id of the Message that this link is a part of. |

## User Specified Entities

Entities are automatically extracted from the content text but there is one case where users and apps can set the entities on an object.

### Links with custom anchor text

If you'd like to provide a link without including the entire URL in the text, you can specify a custom link at Object creation/update. Mentions will still be extracted and your provided links must not overlap with these extracted entities. So you **cannot** have a custom link around a mention.

To prevent phishing, any link where the anchor text differs from the destination domain will be followed by the domain of the link target. In this case, Cellaro will also add the [`amended_len`](#links) field that includes the length of the complete entity and added anti-phishing text. This will make it easier for apps to customize how the anti-phishing protection looks in their apps. **When rendering links in your app, you must show users an indication of where they will end up.**

The `url` value can contain [URI templates](#uri-templates) which the server will process.

#### Example

If you created the following post:

``` js
{
    "text": "View details about this movie on IMDB.",
    "entities": {
        "links": [
            {
                "pos": 33,
                "len": 4,
                "url": "http://imdb.com"
            }
        ]
    }
}
```

Cellaro will store and return:

``` js
{
    "text": "View details about this movie on IMDB[imdb.com].",
    "entities": {
        "links": [
            {
                "pos": 33,
                "len": 4,
                "url": "http://imdb.com",
                "amended_len": 14
            }
        ]
    },
    ...
}
```