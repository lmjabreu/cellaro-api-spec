# Annotations

Annotations are metadata that are attached to an Object when they are created. This allows developers and users to add extra information without interfering with the content of the Object. Annotations will enable developers to build complex Objects and new systems on top of the current Cellaro infrastructure.

## Concretely, what are annotations

Annotations are what power complex Objects with maps, infographics, pictures, videos, sources, etc.

This feature enables new ideas to be built without having to create a brand new API or ecosystem to support the idea.

Let's say an Explore Timeline Event Object needs to include geographic information about a place mentioned in the TV Program, instead of creating multiple types of events, we would include geographic information in an annotation and then clients that support this geographic annotation could show a map.

## Developing with annotations

### Anatomy of an annotation

In general, annotations are a list of objects that have a `type` and a `value`.

``` js
[
    {
        "type": "com.example.awesome",
        "value": {
            "the real annotation data": "would go here"
        }
    }
]
```

<table>
    <tr>
        <th>Field</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>type</code></td>
        <td>string</td>
        <td>A string that looks like a reversed domain name to identify the data format for this annotation. <br><br><div class="alert alert-info">There is no authentication or authorization performed on this field. Just because you create annotations with the type <code>com.example.awesome</code> does not imply you are the only one that is using that namespace or that the <code>value</code> will match the format you expect.</div></td>
    </tr>
    <tr>
        <td><code>value</code></td>
        <td>object</td>
        <td>A complex object containing the metadata for this annotation. <br><br><div class="alert alert-error"><strong>Be extremely careful with this data.</strong> Except for core annotations, no validation is performed on this field.</div></td>
    </tr>
</table>

### Annotations in Cellaro objects

All objects that support annotations (Channel, Program, Event) have an `annotations` field which will contain a list of individual annotation objects if they are present.

#### Example: annotations field in a TV Program

``` js
{
    "annotations": [
        {
            "type": "com.cellaroserver.core.oembed",
            "value": {
                "the real annotation data": "would go here"
            }
        }
    ],
    "id": "270492450-003C505A9FF2EB56D483276A36D45C96",
    "showcased": true,
    "synopsis": "New York City policeman John McClane (Bruce Willis) is visiting his estranged wife (Bonnie Bedelia) and two daughters on Christmas Eve. He joins her at a holiday party in the headquarters of the Japanese-owned business she works for. But the festivities are interrupted by a group of terrorists who take over the exclusive high-rise, and everyone in it. Very soon McClane realizes that there's no one to save the hostages -- but him.",
    "title": "Die Hard"
}
```

### Limits

- Each object is allowed at most 8192 bytes worth of annotations (in total, when serialized as JSON).
- Annotations are immutable and can only be added at creation time.
- An Object can have multiple annotations of the same "type".

### Creating, Updating and Deleting

You can create annotations when you create the object. You must pass JSON objects that include annotations matching the above schema.

To add or update annotations, you pass in the annotations you want to add or update. To delete an annotation, omit the `value` key for the annotation type you want to delete. For example, to delete a geolocation, specify `{"type": "com.cellaroserver.core.geolocation"}`.

### Retrieving

Annotations are always included by default. When making an HTTP request, you can use the parameter `include_annotations=0` to exclude annotations. Disabling Annotations is not supported in the Streaming API.

### Displaying

You are free to choose if and how you render annotations. **Be very careful when consuming this data and do not assume that it follows a specific schema.** Treat data in annotations as untrusted data. Program defensively: your app should not crash or otherwise throw an error if it receives a string where there is usually a dictionary, etc.

### Namespaces

Cellaro will define schemas for common annotation formats. They will live under the `com.cellaroserver.core.*` namespace. This is the only restricted annotation namespace for the `type` field. Any annotation in this namespace must be validated by the API against a [published schema](#core-annotations). Outside of this namespace, developers should create annotations in a reversed-domain namespace of their choosing.

In the `value` of any annotation (including non-core annotations), we treat any key that starts with `+com.cellaroserver.core.*` as a [special replacement value](#annotation-replacement-values). They will be validated against a schema just like the core annotations.

## Documenting annotations

To foster collaboration and adoption, we've set up a github repository for documenting annotations. To discuss annotations, please use the associate issue tracker. For more information on submitting / updating documentation please review the README.

### Core annotations

Annotations that are considered particularly useful and/or well defined may be promoted to "core". As opposed to general annotations, core annotations are validated server-side to match their documented schemas.

We currently define the following core annotations:

| Name | Type | Description |
| ---- | ---- | ----------- |
| [Canonical](/annotations/com.cellaroserver.core.canonical.md) | com.cellaroserver.core.canonical | Specifies the original or canonical source from somewhere else on the web. |
| [Geolocation](/annotations/com.cellaroserver.core.geolocation.md) | com.cellaroserver.core.geolocation | Specifies a geographic point on the Earth. |
| [Language](/annotations/com.cellaroserver.core.language.md) | com.cellaroserver.core.language | Specifies a language. |
| [Concept](/annotations/com.cellaroserver.core.concept.md) | com.cellaroserver.core.concept | Specifies a concept. |
| [Embedded Media](/annotations/com.cellaroserver.core.oembed.md) | com.cellaroserver.core.oembed | Provides information for displaying an image, video, or other rich content. |
| [Offer](/annotations/com.cellaroserver.core.offer.md) | com.cellaroserver.core.offer | Specifies an offer. |
| [Program](/annotations/com.cellaroserver.core.program.md) | com.cellaroserver.core.program | Specifies a program. |
| [Airing](/annotations/com.cellaroserver.core.airing.md) | com.cellaroserver.core.airing | Specifies an airing. |

We will be defining core annotations soon for the following types of data:

* Long-form content
* Attribution and source
* Additional content license grants, where users can opt in to Creative Commons licensing, etc., if desired.
* Content rating, using [MPAA Rating System](http://en.wikipedia.org/wiki/Motion_Picture_Association_of_America_film_rating_system)

Developers are encouraged to create annotations for data not well represented here. If possible, care should be taken not to overlap with existing annotations. Annotations designed to address edge-cases in well-known annotations should include both the well-known annotation and only the augmented parts in the enhancing annotation.

### Annotation replacement values

When Cellaro processes annotation values, any value with a key that starts with `+com.cellaroserver.core.*` will be rewritten based on the core schemas defined below. For example, when attaching a File to a Explore Timeline Event, you might send Cellaro the following annotation:

``` js
{
    "type": "com.example.my_own_annotation",
    "value": {
        "+com.cellaroserver.core.file": {
            "file_token": "12345abcde",
            "format": "oembed",
            "file_id": "1"
        },
        "other_values": "are preserved"
    }
}
```

As explained in the [schema for the `com.cellaroserver.core.file` value](/annotation-replacement-values/+com.cellaroserver.core.file.md), this annotation will be rewritten when the Object is requested from the API:

``` js
{
    "type": "com.example.my_own_annotation",
    "value": {
        "file_id": "1",
        "url": "https://...",
        "url_expires": "2012-01-01T01:00:00Z",
        "version": "1.0",
        "type": "photo",
        ...other oembed values...
        "other_values": "are preserved"
    }
}
```
