## Data formats

### Dates

Dates will be formatted as a **strict** subset of [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601). Dates are parseable by almost any ISO 8601 date parser or merely by parsing from position. All dates will be formatted as follows:

`2012-12-31T13:22:55Z`

Where `2012` is the year, `12` represents December, `31` represents the 31st day of the month, `13` represents 1 PM, `22` minutes and `55` seconds past the hour. All times will be expressed in terms of UTC.

This format was chosen to minimize ambiguity and edge cases in terms of parsing while maximizing readability of dates during development.

### Timestamps

Timestamps will be formatted as follows.

`11:34:09.423`

Where `11` is the hour in 24hour format (HH), `34` the minutes (mm), `09` represents the seconds (ss) and `423` the milliseconds (SSS).

The number of characters in each timestamp division is constant and leading zeros should be used when appropriate ( see the seconds value above ). When not available, the milliseconds value should **not** be ommited but instead **zeroed** like so: `11:34:09.000`.

### Language

Language will be represented as a valid [ISO 639-1](http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) language code.

### Currency

Currency will be represented as a valid [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217) currency code.

### Object IDs

Object IDs will always be transferred as strings to avoid issues with with limitations of JavaScript integers. You cannot assume that Object IDs are integers.

Object IDs aren't normalized at the moment. They're composed with a-z, A-Z, 0-9 and dashes.
